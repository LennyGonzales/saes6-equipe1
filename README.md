# Project Overview

This project is designed to automate and secure the deployment of infrastructure using Ansible and Vagrant, with continuous integration and security checks integrated via GitLab CI.

## Table of Contents
- [Project Overview](#project-overview)
- [Limits](#limits)
- [Prerequisites](#prerequisites)
- [Setup](#setup)
- [Usage](#usage)
    - [Infrastructure](#infrastructure)
    - [SIEM](#siem)
- [CI/CD Pipeline](#cicd-pipeline)
- [Security](#security)
- [Developers](#developers)

## Limits 

- A network where you are the administrator
- Have a server with a public IP address (Cannot add a Continuous Deployment job in the pipeline)
- Not working with Apple Silicon chips (Except with Parallels Pro)

## Prerequisites

Ensure you have the following installed:
- [Vagrant](https://www.vagrantup.com/downloads)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- [Python](https://www.python.org/downloads/)
- [Pip](https://pip.pypa.io/en/stable/installation/)
- [SSH](https://www.ssh.com/ssh/command/)
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Usage
### Infrastructure
- Clone the repository:
    ```sh
    git clone <repository-url>
    cd <repository-directory>
    ```

- To start the Vagrant environment:
    ```sh
    cd src/vagrant/host && vagrant up
    cd ../controller && vagrant up
    ```

- To provision the Vagrant environment using Ansible:
    ```sh
    cd src/ansible && ansible-playbook -i hosts site.yml
    ```

- To destroy the Vagrant environment:
    ```sh
    vagrant destroy
    ```

For the Vagrant configuration, refer to the pdf file named "Configuration_Vagrant" which is in the directory "docs"

### SIEM

In this project, we use the SIEM Elastic Defend to see VM logs.

#### Create an account
Firstly, you have to create an account.

#### Add Elastic Defennd 
Secondly, go to the Search Bar and type "Elastic Defend".
After that, click on "Add Elastic Defend" at the top right of the page, add a name, a description, an agent policy name and tick "Collect system logs and metrics".

#### Create agents and See their logs
Secondly, go to the Search Bar and type "Fleet".
To create a new agent, click on the "Add Agent button", and Copy/Past the last command given by Elastic
```bash
sudo ./elastic-agent install --url=<url> --enrollment-token=<token>
```
You have to do some modifications in this line :
- Add "yes | " at the beginning of the command
- Add "-f" as an argument of the "./elastic-agent install command"

This line should be like this:
```bash
yes | sudo ./elastic-agent install -f --url=<url> --enrollment-token=<token>
```
After clicking on your healthy agent, you can see its details.
You can see its logs by clicking on the "Logs" button

#### Create/Manage rules
A rule is useful to receive an alert through email, Slack, or another connector when the rule's condition is met.

To create and manage rules, you have to click on the "Alerts and rules" button, at the top right of the page


## CI/CD Pipeline

The CI/CD pipeline is defined in the `.gitlab-ci.yml` file and includes the following stages:

- **Lint**: Lints Ansible and Vagrant files.
- **Security**: Scans for secrets and performs security checks.
- **Deploy**: Deploys the application.

### Linting

- **Ansible Lint**: Uses `cytopia/ansible-lint` to lint Ansible playbooks.
- **Vagrant Validate**: Uses `gonzalesl/vagrant:v1.0.0` to validate Vagrant configurations.

### Security

- **Secret Scanning**: Uses `infisical/backend` to scan for secrets.
- **SonarQube Check**: Uses `sonarsource/sonar-scanner-cli:5.0` to perform code quality and security checks.

## Security

Security is a top priority in this project. We use various tools to ensure the codebase is secure and free of vulnerabilities.

- **Secret Scanning**: Automatically scans for secrets in the codebase.
- **SonarQube**: Performs static code analysis to identify potential security issues.


## Developers
- BONNIN Enzo <img align="left" src="https://avatars.githubusercontent.com/u/91955208?v=4" alt="profile" width="20" height="20"/>

- CANALE Enzo <img align="left" src="https://avatars.githubusercontent.com/u/92590811?s=400&u=054f199822e93c3ebefa7c3874aaacaded179e2e&v=4" alt="profile" width="20" height="20"/>

- DOMAGE Hugo <img align="left" src="https://avatars.githubusercontent.com/u/91956673?v=4" alt="profile" width="20" height="20"/>

- GANASSI Alexandre <img align="left" src="https://avatars.githubusercontent.com/u/90609748?v=4" alt="profile" width="20" height="20"/>

- GONZALES Lenny <img align="left" src="https://avatars.githubusercontent.com/u/91269114?s=64&v=4" alt="profile" width="20" height="20"/>

- HOG Joris <img align="left" src="https://avatars.githubusercontent.com/u/148905215?v=4" alt="profile" width="20" height="20"/>

- VIAL Amaury <img align="left" src="https://avatars.githubusercontent.com/u/82141847?v=4" alt="profile" width="20" height="20"/>

- SAADI Nils <img align="left" src="https://avatars.githubusercontent.com/u/91779594?v=4" alt="profile" width="20" height="20"/>

- SAUVA Mathieu <img align="left" src="https://avatars.githubusercontent.com/u/91150750?s=64&v=4" alt="profile" width="20" height="20"/>





